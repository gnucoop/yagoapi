# coding=utf-8

from __future__ import unicode_literals

import shapefile
import zipfile

from base64 import b64decode

try:
    from BytesIO import BytesIO
except ImportError:
    from io import BytesIO

try:
    from urllib.parse import urljoin
except ImportError:
    from urlparse import urljoin

import requests
from requests.exceptions import RequestException

from xml.etree import ElementTree

from django.db import DatabaseError
from django.http.response import HttpResponse
from django.contrib.gis.gdal import GDALException, SpatialReference
from django.contrib.gis.geos import GEOSException, Polygon

from rest_framework.decorators import detail_route
from rest_framework.response import Response
from rest_framework.views import APIView

from simplejson import JSONDecodeError

from geoserver.catalog import Catalog, FailedRequestError,\
    settings as geoserver_settings

from layers.arcgis import arcgisToGeoJSON
from layers.models import ArcGISSource, Layer, LayerTheme, WMSSource
from layers.serializers import ArcGISSourceSerializer,  LayerSerializer, LayerListSerializer,\
    LayerThemeSerializer, WMSSourceSerializer
from layers.shape import GeoJ
from yago.serializers import geometry_to_geojson
from yago.views import BaseModelViewSet


class ImportShapefileView(APIView):
    def post(self, request, format=None):
        name = request.data.get('name', None)
        label = request.data.get('label', None)
        try:
            theme = LayerTheme.objects.get(pk=request.data.get('theme', None))
        except LayerTheme.DoesNotExist:
            theme = None
        shapefile_in = request.data.get('shapefile', None)

        if name is None or label is None or theme is None\
            or shapefile_in is None or 'base64,' not in shapefile_in:
            return Response(status=400)
        
        shapefile_in = shapefile_in.split('base64,')
        if len(shapefile_in) != 2:
            return Response(status=400)
        
        shapefile_in = shapefile_in[1]
        try:
            zip_file = b64decode(shapefile_in)
        except ValueError:
            return Response(status=400)
        
        zb = BytesIO(zip_file)

        shp = None
        shx = None
        dbf = None
        prj = None
        
        with zipfile.ZipFile(zb) as zf:
            for n in zf.namelist():
                extension = n[-4:].lower()
                if extension == '.shp':
                    shp = BytesIO(zf.read(n))
                elif extension == '.shx':
                    shx = BytesIO(zf.read(n))
                elif extension == '.dbf':
                    dbf = BytesIO(zf.read(n))
                elif extension == '.prj':
                    prj = zf.read(n)
        
        if shp is None or shx is None or dbf is None:
            return Response(status=400)
        
        crs = 4326
        if prj is not None:
            res = requests.post('http://prj2epsg.org/search.json', {
                'mode': 'wkt',
                'terms': prj
            }).json()
            if 'codes' in res and 'exact' in res and res['exact']:
                crs = int(res['codes'][0]['code'])
        
        reader = shapefile.Reader(
            shp=shp, shx=shx, dbf=dbf
        )
        fields = reader.fields[1:]
        field_names = [field[0] for field in fields]
        buffer = []
        for sr in reader.shapeRecords():
            atr = dict(zip(field_names, sr.record))
            for k in atr.keys():
                if type(atr[k]) is bytes:
                    atr[k] = atr[k].decode('utf-8', errors='ignore')
            geom = sr.shape.__geo_interface__
            buffer.append(dict(type="Feature",\
                geometry=geom, properties=atr))
        features = {
            'type': 'FeatureCollection',
            'features': buffer,
            'crs': {
                'type': 'name',
                'properties': {
                    'name': 'urn:ogc:def:crs:EPSG::{}'.format(crs)
                }
            }
        }
        xmin = reader.bbox[0]
        ymin = reader.bbox[1]
        xmax = reader.bbox[2]
        ymax = reader.bbox[3]
        if crs != 4326:
            from pyproj import Proj, transform
            p1 = Proj(init='epsg:{}'.format(crs))
            p2 = Proj(init='epsg:{}'.format(4326))
            xmin, ymin = transform(p1, p2, xmin, ymin)
            xmax, ymax = transform(p1, p2, xmax, ymax)
        bbox = 'POLYGON(({} {}, {} {}, {} {}, {} {}, {} {}))'.format(
            xmin, ymin,
            xmin, ymax,
            xmax, ymax,
            xmax, ymin,
            xmin, ymin
        )
        return Response(LayerSerializer(instance=Layer.objects.create(
            layer_type='VECTOR',
            name=name,
            label=label,
            theme=theme,
            bounding_box=bbox,
            features=features
        )).data)


class LayerThemesViewSet(BaseModelViewSet):
    queryset = LayerTheme.objects.all()
    serializer_class = LayerThemeSerializer


class LayersLayersViewSet(BaseModelViewSet):
    queryset = Layer.objects.all()
    serializer_class = LayerSerializer
    list_serializer_class = LayerListSerializer

    @detail_route()
    def get_shapefile(self, request, *args, **kwargs):
        layer = self.get_object()
        if layer.layer_type != 'VECTOR':
            return Response(400)
        
        shp = BytesIO()
        shx = BytesIO()
        dbf = BytesIO()
        zipFile = BytesIO()

        g = GeoJ(layer.features)
        shp_w, prj = g.toShp()

        crs = None
        if 'crs' in layer.features:
            c = layer.features['crs']
            if 'type' in c:
                if c['type'] == 'code':
                    crs = c['properties']['code']
                if c['type'] == 'name':
                    n = c['properties']['name']
                    if 'urn:ogc:def:crs:EPSG::' in n:
                        crs = n.strip('urn:ogc:def:crs:EPSG::')
        if crs is not None:
            prj = requests.get('http://epsg.io/{}.esriwkt'.format(crs)).content
        
        shp_w.saveShp(shp)
        shp_w.saveShx(shx)
        shp_w.saveDbf(dbf)

        with zipfile.ZipFile(zipFile, mode='w') as zf:
            zf.writestr('{}.shp'.format(layer.name), shp.getvalue())
            zf.writestr('{}.shx'.format(layer.name), shx.getvalue())
            zf.writestr('{}.dbf'.format(layer.name), dbf.getvalue())
            zf.writestr('{}.prj'.format(layer.name), prj)
        zf.close()
        
        resp = HttpResponse(zipFile.getvalue(), content_type='application/x-zip-compressed')
        resp['Content-Disposition'] = 'attachment; filename={}.zip'.format(layer.name)

        return resp


class ArcGISSourceViewSet(BaseModelViewSet):
    queryset = ArcGISSource.objects.all()
    serializer_class = ArcGISSourceSerializer

    fmt_suffix = '?f=json'

    def __fmturl__(self, url):
        return '{}{}'.format(url, self.fmt_suffix)
    
    def __get_source_base_url__(self, base_path=None):
        source = self.get_object()
        base_url = urljoin(source.server_url, 'rest/services/')
        if base_path is not None:
            base_url = urljoin(base_url, base_path)
        return base_url

    def __get_catalog_services__(self, catalog):
        services = []
        if 'services' in catalog:
            services = [
                s for s in catalog['services']\
                if s['type'] == 'MapServer' or s['type'] == 'ImageServer'
            ]
        return services
    
    def __get_layer_basic_info__(self, base_path, layer, base_name=None):
        bb = layer['extent']
        name = '{} {}'.format(base_name, layer['name']) if base_name is not None else layer['name']
        xmin = bb['xmin']
        ymin = bb['ymin']
        xmax = bb['xmax']
        ymax = bb['ymax']
        return {
            'name': name,
            'basePath': base_path.strip('/') if base_path is not None else None,
            'id': layer['id'],
            'boundingBox': {
                'type': 'Polygon',
                'crs': {
                    'type': 'EPSG',
                    'properties': {
                        'code': 4326
                    }
                },
                'coordinates': [[
                    [xmin, ymin], [xmin, ymax], [xmax, ymax], [xmax, ymin], [xmin, ymin]
                ]]
            },
            'layerType': 'VECTOR'
        }
    
    def __get_map_service_layer__(self, base_path, layer, base_name=None):
        if layer['type'] == 'Feature Layer':
            return [self.__get_layer_basic_info__(base_path, layer, base_name)]
        elif layer['type'] == 'Group Layer':
            name = '{} {}'.format(base_name, layer['name']) if base_name is not None else layer['name']
            for l in layer['subLayers']:
                url = self.__fmturl__(urljoin(
                    self.__get_source_base_url__(base_path),
                    'MapServer/{}/'.format(l['id'])
                ))
                try:
                    return self.__get_map_service_layer__(
                        base_path,
                        requests.get(url).json(),
                        name
                    )
                except (RequestException, JSONDecodeError) as e:
                    return []
        else:
            return []
    
    def __get_map_service_layers__(self, base_path):
        layers = []
        service_url = urljoin(
            self.__get_source_base_url__(base_path),
            'MapServer/layers'
        )
        url = self.__fmturl__(service_url)
        try:
            res = requests.get(url).json()
            if 'layers' not in res:
                return layers
            for layer in res['layers']:
                layers += self.__get_map_service_layer__(base_path, layer)
        except (RequestException, JSONDecodeError):
            pass
        return layers
        

    def __get_source_layers__(self, base_path=None):
        layers = []
        url = self.__fmturl__(self.__get_source_base_url__(base_path))
        try:
            catalog = requests.get(url).json()
            services = self.__get_catalog_services__(catalog)
            for service in services:
                service_name = '{}/'.format(service['name'])
                if service['type'] == 'MapServer':
                    if base_path is None:
                        bp = service_name
                    else:
                        bp = urljoin(base_path, service_name)
                    layers += self.__get_map_service_layers__(bp)
        except (RequestException, JSONDecodeError):
            pass
        return layers

    @detail_route()
    def available_layers(self, request, *args, **kwargs):
        layers = self.__get_source_layers__()
        return Response(sorted(layers, key=lambda s: s['name'].lower()))
    
    def __import_available_layer__(self, layer, data):
        import_name = self.request.data.get('name', None)
        import_label = self.request.data.get('label', None)
        import_bbox = self.request.data.get('bounding_box', None)
        try:
            import_theme = LayerTheme.objects.get(pk=self.request.data.get('theme', None))
        except LayerTheme.DoesNotExist:
            import_theme = None
        if import_name is None or import_label is None\
                or import_theme is None or import_bbox is None:
            return Response(status=400)
        workspace = layer['basePath']
        name = '{}'.format(layer['id'])
        is_vector = layer['layerType'] == 'VECTOR'
        c = layer['boundingBox']['coordinates'][0]
        bbox = 'POLYGON(({} {}, {} {}, {} {}, {} {}, {} {}))'.format(
            c[0][0], c[0][1], c[1][0], c[1][1], c[2][0], c[2][1],
            c[3][0], c[3][1], c[4][0], c[4][1]
        )
        try:
            return Response(LayerSerializer(instance=Layer.objects.create(
                layer_type=layer['layerType'],
                name=import_name,
                label=import_label,
                theme=import_theme,
                source_type='ARCGIS',
                bounding_box=bbox,
                features=layer['features'] if is_vector else None,
                url=self.get_object().server_url,
                source_name='{}:{}'.format(workspace, name)
            )).data)
        except (GDALException, GEOSException, DatabaseError) as e:
            return Response(status=400)
    
    @detail_route(methods=['get', 'post'], url_path='available_layer/(?P<name>[^/.]+)')
    def available_layer(self, request, name=None, *args, **kwargs):
        base_path, layer_id = name.split(':')
        url = urljoin(
            '{}/MapServer/'.format(self.__get_source_base_url__(base_path)),
            layer_id
        )
        try:
            res = requests.get(self.__fmturl__(url)).json()
            layer = self.__get_layer_basic_info__(base_path, res)
            url = '{}/query?where=objectid+%3D+objectid&outfields=*&f=json'.format(url)
            esri_json = requests.get(url).json()
            if 'features' in esri_json:
                features = []
                for feature in esri_json['features']:
                    features.append(arcgisToGeoJSON(feature))
                layer['features'] = {
                    'type': 'FeatureCollection',
                    'crs': {
                        'type': 'EPSG',
                        'properties': {
                            'code': 4326
                        }
                    },
                    'features': features
                }
            if request.method == 'GET':
                return Response(layer)
            else:
                return self.__import_available_layer__(layer, request.data)
        except (RequestException, JSONDecodeError) as e:
            pass
        return Response(status=400)
    


class WMSSourceViewSet(BaseModelViewSet):
    queryset = WMSSource.objects.all()
    serializer_class = WMSSourceSerializer

    def __get_source_catalog__(self, source):
        s_u = source.username
        s_p = source.password
        username = s_u if s_u is not None and len(s_u) > 0\
            else geoserver_settings.DEFAULT_USERNAME
        password = s_p if s_p is not None and len(s_p) > 0\
            else geoserver_settings.DEFAULT_PASSWORD
        return Catalog(
            '{}/rest/'.format(source.server_url),
            username=username,
            password=password
        )
    
    def __get_source_layer__(self, source, name):
        catalog = self.__get_source_catalog__(source)
        try:
            return catalog.get_layer(name)
        except (RequestException, FailedRequestError) as e:
            return None
    
    @staticmethod
    def __get_xml_element_name__(el, namespace):
        return '{}{}'.format(
            '{{{}}}'.format(namespace) if namespace is not None else '',
            el
        )
    
    @staticmethod
    def __get_capabilities_ns__(caps):
        def_ns = None
        ns_parts = caps.tag.split('}')
        if len(ns_parts) > 1:
            def_ns = ns_parts[0].strip('{')
        return def_ns

    def __get_source_layers__(self, source):
        source_layers = []
        url = '{}?service=WMS&version=1.1.1&request=GetCapabilities{}'.format(
            source.ows_server_url,
            '&namespace={}'.format(source.workspace) if source.workspace is not None else ''
        )
        try:
            res = requests.get(url)
            caps = ElementTree.fromstring(res.content)
            def_ns = self.__get_capabilities_ns__(caps)
            cap = caps.find(self.__get_xml_element_name__('Capability', def_ns))
            if cap is None:
                return source_layers
            layers = cap.find(self.__get_xml_element_name__('Layer', def_ns))
            ls = layers.findall(self.__get_xml_element_name__('Layer', def_ns))
            if ls is None:
                return source_layers
            for layer in ls:
                bb = layer.find(self.__get_xml_element_name__('EX_GeographicBoundingBox', def_ns))
                if bb is None:
                    bb = layer.find(self.__get_xml_element_name__('LatLonBoundingBox', def_ns))
                else:
                    wlon = bb.find(self.__get_xml_element_name__('westBoundLongitude', def_ns))
                    slat = bb.find(self.__get_xml_element_name__('southBoundLatitude', def_ns))
                    elon = bb.find(self.__get_xml_element_name__('eastBoundLongitude', def_ns))
                    nlat = bb.find(self.__get_xml_element_name__('northBoundLatitude', def_ns))
                    if wlon is None or slat is None or elon is None or nlat is None:
                        break
                    bbox = [
                        float(wlon.text), float(slat.text),
                        float(elon.text), float(nlat.text),
                        4326
                    ]
                if bb is None:
                    break
                else:
                    coords = bb.attrib
                    if 'minx' not in coords or 'miny' not in coords\
                        or 'maxx' not in coords or 'maxy' not in coords:
                        break
                    bbox = [
                        float(coords['minx']), float(coords['miny']),
                        float(coords['maxx']),float(coords['maxy']),
                        4326
                    ]
                
                lname = layer.find(self.__get_xml_element_name__('Name', def_ns))
                if lname is None:
                    break
                
                if ':' in lname.text:
                    np = lname.text.split(':')
                    workspace = np[0]
                    name = np[1]
                else:
                    workspace = None
                    name = lname.text
                
                url = '{}?service=WMS&version=1.1.1&request=DescribeLayer&layers={}'.format(
                    source.ows_server_url, lname.text
                )
                try:
                    res = requests.get(url)
                    dl = ElementTree.fromstring(res.content)
                    if dl.tag == 'ServiceExceptionReport':
                        layer_type = 'VECTOR'
                    else:
                        ld = dl.find(self.__get_xml_element_name__('LayerDescription', def_ns))
                        if ld is None:
                            break
                        layer_type = 'VECTOR' if 'wfs' in ld.attrib else 'WMS_RASTER'
                except (RequestException, ElementTree.ParseError):
                    break
                
                source_layers.append({
                    'workspace': workspace,
                    'name': name,
                    'layerType': layer_type,
                    'boundingBox': self.__bbox_to_polygon__(bbox)
                })

            return source_layers
        except (RequestException, ElementTree.ParseError) as e:
            print(e)
            return source_layers
    
    def __bbox_to_polygon__(self, bbox):
        try:
            polygon = Polygon.from_bbox(
                (bbox[0], bbox[2], bbox[1], bbox[3])
            )
            polygon.srid = SpatialReference(bbox[4]).srid
            return geometry_to_geojson(polygon)
        except GDALException:
            return None

    def __import_available_layer__(self, source, layer, data):
        import_name = self.request.data.get('name', None)
        import_label = self.request.data.get('label', None)
        import_bbox = self.request.data.get('bounding_box', None)
        try:
            import_theme = LayerTheme.objects.get(pk=self.request.data.get('theme', None))
        except LayerTheme.DoesNotExist:
            import_theme = None
        if import_name is None or import_label is None\
                or import_theme is None or import_bbox is None:
            return Response(status=400)
        workspace = layer.get('workspace')
        name = layer.get('name')
        layer_type = layer.get('layerType')
        is_vector = layer_type == 'VECTOR'
        bounding_box = layer.get('boundingBox')
        bbox = 'POLYGON(({} {}, {} {}, {} {}, {} {}, {} {}))'.format(
            *[i for y in bounding_box.get('coordinates')[0] for i in y]
        )
        try:
            return Response(LayerSerializer(instance=Layer.objects.create(
                layer_type=layer_type,
                name=import_name,
                label=import_label,
                theme=import_theme,
                source_type='WMS',
                bounding_box=bbox,
                features=self.__get_layer_features__(source, workspace, name)\
                if is_vector else None,
                url=self.get_object().server_url,
                source_name='{}:{}'.format(workspace, name)
            )).data)
        except (GDALException, GEOSException, DatabaseError) as e:
            print(e)
            return Response(status=400)
    
    def __get_layer_features__(self, source, workspace, name):
        try:
            return requests.get(source.ows_server_url, {
                'service': 'WFS',
                'version': '1.1.1',
                'request': 'GetFeature',
                'typeNames': '{}:{}'.format(workspace, name) if workspace is not None else name,
                'outputFormat': 'json'
            }).json()
        except RequestException:
            return None
    
    def __get_layer_type__(self, layer):
        layer_type = layer.dom.find('type').text
        if layer_type == 'VECTOR':
            return 'VECTOR'
        return 'WMS_RASTER'

    @detail_route(methods=['get', 'post'], url_path='available_layer/(?P<name>[^/.]+)')
    def available_layer(self, request, name=None, *args, **kwargs):
        source = self.get_object()
        if ':' in name:
            tgt_workspace, tgt_layer = name.split(':')
        else:
            tgt_workspace = None
            tgt_layer = name
        layers = [
            l for l in self.__get_source_layers__(source)\
            if l['workspace'] == tgt_workspace and l['name'] == tgt_layer
        ]
        if len(layers) == 1:
            l = layers[0]
            qs = Layer.objects.filter(
                url=source.ows_server_url,
                source_name__endswith=':{}'.format(l['name']),
                deleted=False
            )
            if not qs.exists():
                try:
                    if request.method == 'GET':
                        l.update({
                            'features': self.__get_layer_features__(source, tgt_workspace, tgt_layer)\
                            if l['layerType'] == 'VECTOR' else None
                        })
                        return Response(l)
                    else:
                        return self.__import_available_layer__(source, l, request.data)
                except (AttributeError, RequestException, FailedRequestError) as e:
                    print(e)
        return Response('', status=404)
        
    @detail_route()
    def available_layers(self, request, *args, **kwargs):
        source = self.get_object()
        layers = self.__get_source_layers__(source)
        return Response(sorted(layers, key=lambda s: s['name'].lower()))


class SpatialAnalysisView(APIView):
    def post(self, request, format=None):
        from openpyxl import Workbook
        from openpyxl.chart import BarChart, PieChart, Reference

        labels = request.data.get('labels', None)
        colors = request.data.get('colors', None)
        values = request.data.get('values', None)
        chart = request.data.get('chart', None)
        indicator = request.data.get('indicator', None)

        cols_num = len(labels)
        if labels is None or colors is None or chart is None or\
            cols_num != len(values):
            return Response(status=400)
        
        wb = Workbook()
        ws = wb.active

        if chart == 'bar':
            ws.append([None] + labels)
            ws.append([indicator] + values)
            
            chart = BarChart()
            
            data = Reference(ws, min_col=2, min_row=1, max_col=cols_num + 1, max_row=2)
            cats = Reference(ws, min_col=1, min_row=2)
            chart.add_data(data, titles_from_data=True)
            chart.set_categories(cats)
        else:
            for i in range(0, cols_num):
                ws.append([labels[i], values[i]])
            
            chart = PieChart()
            
            data = Reference(ws, min_col=2, min_row=1, max_col=2, max_row=cols_num)
            cats = Reference(ws, min_col=1, min_row=1, max_col=1, max_row=cols_num)
            chart.add_data(data)
            chart.set_categories(cats)
            chart.title = indicator

        ws.add_chart(chart, "E5")

        response = HttpResponse(
            content_type='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
        )
        response['Content-Disposition'] = 'attachment; filename=spatial_analysis.xlsx'
        wb.save(response)

        return response
