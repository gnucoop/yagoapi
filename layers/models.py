# coding=utf-8

from __future__ import unicode_literals
from future.utils import python_2_unicode_compatible

from django.conf import settings
from django.contrib.gis.db import models
from django.contrib.gis.geos import Polygon
from django.utils.translation import ugettext as _

from jsonfield import JSONField

from yago.models import BaseModel


@python_2_unicode_compatible
class ArcGISSource(BaseModel):
    name = models.CharField(max_length=255)
    server_url = models.CharField(max_length=255)
    service = models.CharField(max_length=255, blank=True, null=True, default=None)
    username = models.CharField(max_length=255, blank=True, null=True, default=None)
    password = models.CharField(max_length=255, blank=True, null=True, default=None)


@python_2_unicode_compatible
class WMSSource(BaseModel):
    name = models.CharField(max_length=255)
    server_url = models.CharField(max_length=255)
    workspace = models.CharField(max_length=255, blank=True, null=True, default=None)
    username = models.CharField(max_length=255, blank=True, null=True, default=None)
    password = models.CharField(max_length=255, blank=True, null=True, default=None)

    def __str__(self):
        return self.name

    @property
    def ows_server_url(self):
        server_url = self.server_url.strip('/')
        if server_url[-9:] == 'WMSServer':
            return server_url
        last_part = server_url[-3:]
        valid_parts = ['ows', 'wms']
        if last_part not in valid_parts:
            server_url = '{}/ows'.format(server_url)
        return server_url


@python_2_unicode_compatible
class LayerTheme(BaseModel):
    name = models.CharField(max_length=255)
    parent = models.ForeignKey('layers.LayerTheme', blank=True, null=True, default=None)

    class Meta:
        ordering = ['id']

    @property
    def parent_name(self):
        return self.parent.name if self.parent is not None else ''

    def __str__(self):
        return self.name


@python_2_unicode_compatible
class Layer(BaseModel):
    LAYER_TYPES = (
        ('TILE', _('Tile')),
        ('VECTOR', _('Vector')),
        ('WMS_RASTER', _('WMS Raster')),
    )

    LAYER_SOURCE_TYPES = (
        ('WMS', _('WMS')),
        ('ARCGIS', _('ArcGIS'))
    )

    name = models.CharField(max_length=255, db_index=True)
    label = models.CharField(max_length=255)
    theme = models.ForeignKey('layers.LayerTheme', blank=True, null=True, default=None)
    layer_type = models.CharField(max_length=10, choices=LAYER_TYPES)
    bounding_box = models.PolygonField(default=Polygon.from_bbox((0, 0, 0, 0)))
    features = JSONField(null=True, blank=True)
    source_type = models.CharField(
        max_length=255, choices=LAYER_SOURCE_TYPES,
        db_index=True, blank=True, null=True, default=None
    )
    url = models.CharField(max_length=255, db_index=True, blank=True, null=True, default=None)
    source_name = models.CharField(max_length=255, db_index=True, blank=True, null=True, default=None)
    metadata = models.FileField(upload_to=settings.METADATA_PATH, null=True, blank=True)
    data_labels = JSONField(null=True, blank=True)

    @property
    def theme_name(self):
        return self.theme.name if self.theme is not None else ''

    def __str__(self):
        return self.label
