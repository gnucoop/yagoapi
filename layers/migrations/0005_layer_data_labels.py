# -*- coding: utf-8 -*-
# Generated by Django 1.10.5 on 2017-03-21 07:23
from __future__ import unicode_literals

from django.db import migrations
import jsonfield.fields


class Migration(migrations.Migration):

    dependencies = [
        ('layers', '0004_layer_metadata'),
    ]

    operations = [
        migrations.AddField(
            model_name='layer',
            name='data_labels',
            field=jsonfield.fields.JSONField(blank=True, null=True),
        ),
    ]
