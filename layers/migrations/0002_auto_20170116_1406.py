# -*- coding: utf-8 -*-
# Generated by Django 1.10.4 on 2017-01-16 14:06
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('layers', '0001_initial'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='layer',
            name='wms_source',
        ),
        migrations.AddField(
            model_name='layer',
            name='source_name',
            field=models.CharField(blank=True, db_index=True, default=None, max_length=255, null=True),
        ),
        migrations.AddField(
            model_name='layer',
            name='url',
            field=models.CharField(blank=True, db_index=True, default=None, max_length=255, null=True),
        ),
        migrations.AlterField(
            model_name='layer',
            name='layer_type',
            field=models.CharField(choices=[('TILE', 'Tile'), ('VECTOR', 'Vector'), ('WMS_RASTER', 'WMS Raster')], max_length=6),
        ),
    ]
