# coding=utf-8

from __future__ import unicode_literals

from rest_framework.fields import CharField
from rest_framework.serializers import ModelSerializer

from layers.models import ArcGISSource, Layer, LayerTheme, WMSSource
from yago.serializers import B64FileField, GeoModelSerializer


class LayerThemeSerializer(ModelSerializer):
    parent_name = CharField(read_only=True)
    
    class Meta:
        model = LayerTheme
        fields = '__all__'


class LayerSerializer(GeoModelSerializer):
    metadata = B64FileField(required=False, allow_null=True)
    theme_name = CharField(read_only=True)
    
    class Meta:
        model = Layer
        fields = '__all__'


class LayerListSerializer(LayerSerializer):
    class Meta:
        model = Layer
        exclude = ('bounding_box', 'features', )


class WMSSourceSerializer(ModelSerializer):
    class Meta:
        model = WMSSource
        fields = '__all__'


class ArcGISSourceSerializer(ModelSerializer):
    class Meta:
        model = ArcGISSource
        fields = '__all__'
