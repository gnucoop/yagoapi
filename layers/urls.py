# coding=utf-8

from __future__ import unicode_literals

from django.conf.urls import url

from rest_framework.routers import DefaultRouter

from layers.views import ArcGISSourceViewSet, ImportShapefileView,\
    LayersLayersViewSet, LayerThemesViewSet,\
    SpatialAnalysisView, WMSSourceViewSet


router = DefaultRouter()
router.register(r'themes', LayerThemesViewSet, 'layers-themes')
router.register(r'layers', LayersLayersViewSet, 'layers-layers')
router.register(r'wms_sources', WMSSourceViewSet, 'layers-wms_sources')
router.register(r'arcgis_sources', ArcGISSourceViewSet, 'layers-arcgis_sources')
urlpatterns = router.urls + [
    url(r'import_shapefile', ImportShapefileView.as_view(), name='layers-import_shapefile'),
    url(r'spatial_analysis', SpatialAnalysisView.as_view(), name='layers-spatial_analysis')
]
