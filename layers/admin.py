# coding=utf-8

from __future__ import unicode_literals

from django.contrib.admin import ModelAdmin, register

from layers.forms import LayerForm
from layers.models import ArcGISSource, Layer, LayerTheme, WMSSource


@register(Layer)
class LayerAdmin(ModelAdmin):
    form = LayerForm
    list_display = ('label', 'layer_type', 'theme', )

@register(LayerTheme)
class LayerThemeAdmin(ModelAdmin):
    list_display = ('name', 'parent', )

@register(WMSSource)
class WMSSourceAdmin(ModelAdmin):
    pass

@register(ArcGISSource)
class ArcGISSourceAdmin(ModelAdmin):
    pass
