# checks if 2 x,y points are equal
def pointsEqual(a, b):
    for i in range(0, len(a)):
        if a[i] != b[i]:
            return False
    return True

# checks if the first and last points of a ring are equal and closes the ring
def closeRing(coordinates):
    if not pointsEqual(coordinates[0], coordinates[-1:]):
        coordinates.append(coordinates[0])
    return coordinates

# determine if polygon ring coordinates are clockwise. clockwise signifies outer ring, counter-clockwise an inner ring
# or hole. this logic was found at http://stackoverflow.com/questions/1165647/how-to-determine-if-a-list-of-polygon-
# points-are-in-clockwise-order
def ringIsClockwise(ringToTest):
    total = 0
    rLength = len(ringToTest)
    pt1 = ringToTest[0]
    for i in range(0, rLength - 1):
        pt2 = ringToTest[i + 1]
        total += (pt2[0] - pt1[0]) * (pt2[1] + pt1[1])
        pt1 = pt2
    return total >= 0

# ported from terraformer.js https://github.com/Esri/Terraformer/blob/master/terraformer.js#L504-L519
def vertexIntersectsVertex(a1, a2, b1, b2):
    uaT = ((b2[0] - b1[0]) * (a1[1] - b1[1])) - ((b2[1] - b1[1]) * (a1[0] - b1[0]))
    ubT = ((a2[0] - a1[0]) * (a1[1] - b1[1])) - ((a2[1] - a1[1]) * (a1[0] - b1[0]))
    uB = ((b2[1] - b1[1]) * (a2[0] - a1[0])) - ((b2[0] - b1[0]) * (a2[1] - a1[1]))

    if uB != 0:
        ua = uaT / uB
        ub = ubT / uB

        if ua >= 0 and ua <= 1 and ub >= 0 and ub <= 1:
            return True

    return False

# ported from terraformer.js https://github.com/Esri/Terraformer/blob/master/terraformer.js#L521-L531
def arrayIntersectsArray(a, b):
  for i in range(0, len(a) - 1):
      for j in range(0, len(b) - 1):
          if vertexIntersectsVertex(a[i], a[i + 1], b[j], b[j + 1]):
              return True
  return False

# ported from terraformer.js https://github.com/Esri/Terraformer/blob/master/terraformer.js#L470-L480
def coordinatesContainPoint(coordinates, point):
    contains = False
    i = 1
    l = len(coordinates)
    j = l - 1
    for i in range(0, len(coordinates)):
        if ((coordinates[i][1] <= point[1] and point[1] < coordinates[j][1]) or\
            (coordinates[j][1] <= point[1] and point[1] < coordinates[i][1])) and\
            (point[0] < (((coordinates[j][0] - coordinates[i][0]) *\
            (point[1] - coordinates[i][1])) / (coordinates[j][1] - coordinates[i][1]))\
            + coordinates[i][0]):
            contains = not contains
        j = i
    return contains

# ported from terraformer-arcgis-parser.js https://github.com/Esri/terraformer-arcgis-parser/blob/master/terraformer-arcgis-parser.js#L106-L113
def coordinatesContainCoordinates(outer, inner):
    intersects = arrayIntersectsArray(outer, inner)
    contains = coordinatesContainPoint(outer, inner[0])
    if not intersects and contains:
        return True
    return False

# do any polygons in this array contain any other polygons in this array?
# used for checking for holes in arcgis rings
# ported from terraformer-arcgis-parser.js https://github.com/Esri/terraformer-arcgis-parser/blob/master/terraformer-arcgis-parser.js#L117-L172
def convertRingsToGeoJSON(rings):
    outerRings = []
    holes = []
    
    # for each ring
    for r in range(0, len(rings)):
        ring = closeRing(rings[r][:])
        if len(ring) >= 4:
            # is this ring an outer ring? is it clockwise?
            if ringIsClockwise(ring):
                polygon = [ring]
                outerRings.append(polygon)  # push to outer rings
            else:
                holes.append(ring)  # counterclockwise push to holes

    uncontainedHoles = []

    # while there are holes left...
    while len(holes) > 0:
        # pop a hole off out stack
        hole = holes.pop()

        # loop over all outer rings and see if they contain our hole.
        contained = False
        x = len(outerRings) - 1
        while x >= 0:
            outerRing = outerRings[x][0]
            if coordinatesContainCoordinates(outerRing, hole):
                # the hole is contained push it into our polygon
                outerRings[x].append(hole)
                contained = True
                break
            x -= 1

            if not contained:
                uncontainedHoles.appen(hole)

    # if we couldn't match any holes using contains we can try intersects...
    while len(uncontainedHoles) > 0:
        # pop a hole off out stack
        hole = uncontainedHoles.pop()

        # loop over all outer rings and see if any intersect our hole.
        intersects = False

        x = len(outerRings)
        while x >= 0:
            outerRing = outerRings[x][0]
            if arrayIntersectsArray(outerRing, hole):
                outerRings[x].push(hole)
                intersects = True
                break
            x -= 1

        if not intersects:
            outerRings.push([hole[::-1]])

    if len(outerRings) == 1:
        return {
            'type': 'Polygon',
            'coordinates': outerRings[0]
        }
    else:
        return {
            'type': 'MultiPolygon',
            'coordinates': outerRings
        }

# This function ensures that rings are oriented in the right directions
# outer rings are clockwise, holes are counterclockwise
# used for converting GeoJSON Polygons to ArcGIS Polygons
def orientRings(poly):
    output = []
    polygon = poly[:]
    outerRing = closeRing(polygon.pop(0)[:])
    if len(outerRing) >= 4:
        if not ringIsClockwise(outerRing):
            outerRing[::-1]

    output.append(outerRing)

    for i in range(0, len(polygon)):
        hole = closeRing(polygon[i][:])
        if len(hole) >= 4:
            if ringIsClockwise(hole):
                hole[::-1]
            output.append(hole)
    return output

# This function flattens holes in multipolygons to one array of polygons
# used for converting GeoJSON Polygons to ArcGIS Polygons
def flattenMultiPolygonRings(rings):
  output = []
  for ring in rings:
      polygon = orientRings(ring)
      x = len(polygon)
      while x >= 0:
          ring = polygon[x][:]
          output.append(ring)
          x -= 1
  return output

# shallow object clone for feature properties and attributes
# from http://jsperf.com/cloning-an-object/2
def shallowClone(obj):
  target = dict()
  for i in obj.keys():
      target[i] = obj[i]
  return target

def arcgisToGeoJSON(arcgis, idAttribute=None):
    geojson = dict()

    if 'x' in arcgis and 'y' in arcgis\
        and type(arcgis['x']) is float and type(arcgis['y']) is float:
        geojson['type'] = 'Point'
        geojson['coordinates'] = [arcgis['x'], arcgis['y']]

    if 'points' in arcgis:
        geojson['type'] = 'MultiPoint'
        geojson['coordinates'] = arcgis['points'][:]
    
    if 'paths' in arcgis:
        if len(arcgis['paths'] == 1):
            geojson['type'] = 'LineString'
            geojson['coordinates'] = arcgis['paths'][0][:]
        else:
            geojson['type'] = 'MultiLineString'
            geojson['coordinates'] = arcgis['paths'][:]
    
    if 'rings' in arcgis:
        geojson = convertRingsToGeoJSON(arcgis['rings'][:])
    
    if 'geometry' in arcgis or 'attributes' in arcgis:
        geojson['type'] = 'Feature'
        geojson['geometry'] = arcgisToGeoJSON(arcgis['geometry'])\
            if 'geometry' in arcgis else None
        geojson['properties'] = shallowClone(arcgis['attributes'])\
            if 'attributes' in arcgis else None
        if 'attributes' in arcgis:
            if idAttribute is not None and idAttribute in arcgis['attributes']:
                geojson['id'] = arcgis['attributes'][idAttribute]
            elif 'OBJECTID' in arcgis['attributes']:
                geojson['id'] = arcgis['attributes']['OBJECTID']
            elif 'FID' in arcgis['attributes']:
                geojson['id'] = arcgis['attributes']['FID']

    return geojson

def geojsonToArcGIS(geojson, idAttribute=None):
    if idAttribute is None:
        idAttribute = 'OBJECTID'
    spatialReference = { 'wkid': 4326 }
    result = dict()

    geo_type = geojson['type']
    if geo_type == 'Point':
        result['x'] = geojson['coordinates'][0]
        result['y'] = geojson['coordinates'][1]
        result['spatialReference'] = spatialReference
    elif geo_type == 'MultiPoint':
        result['points'] = geojson['coordinates'][:]
        result['spatialReference'] = spatialReference
    elif geo_type == 'LineString':
        result['paths'] = [geojson['coordinates'][:]]
        result['spatialReference'] = spatialReference
    elif geo_type == 'MultiLineString':
        result['paths'] = geojson['coordinates'][:]
        result['spatialReference'] = spatialReference
    elif geo_type == 'Polygon':
        result.rings = orientRings(geojson['coordinates'][:])
        result['spatialReference'] = spatialReference
    elif geo_type == 'MultiPolygon':
        result.rings = flattenMultiPolygonRings(geojson['coordinates'][:])
        result['spatialReference'] = spatialReference
    elif geo_type == 'Feature':
        if 'geometry' in geojson:
            result.geometry = geojsonToArcGIS(geojson.geometry, idAttribute)
        result['attributes'] = shallowClone(geojson['properties'])\
            if 'properties' in geojson else dict()
        if 'id' in geojson:
            result['attributes'][idAttribute] = geojson['id']
    elif geo_type == 'FeatureCollection':
        array_result = []
        for feature in geojson['features']:
            array_result.append(geojsonToArcGIS(feature, idAttribute))
        return array_result
    elif geo_type == 'GeometryCollection':
        array_result = []
        for geometry in geojson['geometries']:
            array_result.append(geojsonToArcGIS(geometry, idAttribute))
        return array_result
    return result
