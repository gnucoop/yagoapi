from django.contrib.gis.forms import OpenLayersWidget, PolygonField
from django.forms import ModelForm

from layers.models import Layer


class LayerForm(ModelForm):
    bounding_box = PolygonField(widget=OpenLayersWidget(attrs={
        'display_raw': True
    }))

    class Meta:
        model = Layer
        fields = '__all__'
