# coding=utf-8

from __future__ import unicode_literals

from django.conf.urls import url

from rest_framework.routers import DefaultRouter

from rest_framework_jwt.views import obtain_jwt_token, refresh_jwt_token

from rest_auth.views import PasswordResetView, PasswordResetConfirmView

from accounts.views import InvalidateTokenView, MeView,\
    UsernameExistsView, EmailExistsView, RegisterView, UserViewSet

router = DefaultRouter()
router.register(r'users', UserViewSet)

urlpatterns = [
    url(r'^api-token-auth/', obtain_jwt_token),
    url(r'^api-token-refresh/', refresh_jwt_token),
    url(r'^api-token-invalidate/', InvalidateTokenView.as_view()),
    url(r'^me/', MeView.as_view()),
    url(r'^username_exists/(?P<username>[\w]+)', UsernameExistsView.as_view()),
    url(r'^email_exists/(?P<email>[[\w.%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4})',
        EmailExistsView.as_view()),
    url(r'^register/', RegisterView.as_view()),
    url(r'^recover_password/', PasswordResetView.as_view()),
    url(r'^reset_password/', PasswordResetConfirmView.as_view())
] + router.urls
