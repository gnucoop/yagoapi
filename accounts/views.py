# coding=utf-8

from __future__ import unicode_literals

from django.contrib.auth import get_user_model

from rest_framework import status 
from rest_framework.permissions import AllowAny, IsAdminUser
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.viewsets import ModelViewSet

from rest_framework_jwt.serializers import  JSONWebTokenSerializer
from rest_framework_jwt.views import jwt_response_payload_handler

from accounts.serializers import AdminUserSerializer, UserSerializer


class InvalidateTokenView(APIView):
    def get(self, request, *args, **kwargs):
        return self.__response__()
    
    def post(self, request, *args, **kwargs):
        return self.__response__()
    
    def __response__(self):
        return Response({'success': True})


class MeView(APIView):
    def get(self, request, *args, **kwargs):
        return Response(UserSerializer(request.user).data)


class UsernameExistsView(APIView):
    def get(self, request, username='', *args, **kwargs):
        um = get_user_model()
        return Response({
            'exists': um.objects.filter(username=username).exists()
        })


class EmailExistsView(APIView):
    def get(self, request, email='', *args, **kwargs):
        um = get_user_model()
        return Response({
            'exists': um.objects.filter(email=email).exists()
        })


class RegisterView(APIView):
    serializer_class = UserSerializer
    permission_classes = (AllowAny, )

    def post(self, request, format=None):
        serializer = self.serializer_class(data=request.data)
        if serializer.is_valid():
            serializer.save()
            token_serializer = JSONWebTokenSerializer(data={
                'username': request.data['username'],
                'password': request.data['password']
            })
            if token_serializer.is_valid():
                user = token_serializer.object.get('user') or request.user
                token = token_serializer.object.get('token')
                response_data = jwt_response_payload_handler(token, user, request)
                return Response(response_data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status.HTTP_400_BAD_REQUEST)


class UserViewSet(ModelViewSet):
    queryset = get_user_model().objects.all()
    permission_classes = (IsAdminUser, )
    serializer_class = AdminUserSerializer
