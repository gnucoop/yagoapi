# coding=utf-8

from __future__ import unicode_literals

from copy import deepcopy

from django.contrib.auth import get_user_model

from rest_framework.serializers import CharField, ModelSerializer, ValidationError


class UserSerializer(ModelSerializer):
    password = CharField(write_only=True, required=True)
    confirm_password = CharField(write_only=True, required=True)

    class Meta:
        model = get_user_model()
        fields = '__all__'
    
    def create(self, validated_data):
        data = deepcopy(validated_data)
        if 'confirm_password' in data:
            del data['confirm_password']
        return self.Meta.model.objects.create_user(**data)
    
    def validate(self, data):
        password = data['password'] if 'password' in data else None
        confirm_password = data['confirm_password'] if 'confirm_password' in data else None
        if password is not None and confirm_password is not None and password != confirm_password:
            raise ValidationError('The passwords do not match')
        return data


class AdminUserSerializer(UserSerializer):
    password = CharField(write_only=True, required=False, allow_null=True)
    confirm_password = CharField(write_only=True, required=False, allow_null=True)

    class Meta:
        model = get_user_model()
        fields = '__all__'
    
    def update(self, instance, validated_data):
        data = deepcopy(validated_data)
        password = None
        confirm_password = None
        if 'password' in data:
            password = data['password']
            del data['password']
        if 'confirm_password' in data:
            confirm_password = data['confirm_password']
            del data['confirm_password']
        if password is not None and password == confirm_password:
            instance.set_password(password)
        return super(AdminUserSerializer, self).update(instance, data)
