# coding=utf-8

from __future__ import unicode_literals

from rest_framework.serializers import ModelSerializer, JSONField

from maps.models import Map


class MapSerializer(ModelSerializer):
    content = JSONField()

    class Meta:
        model = Map
        fields = '__all__'


class MapListSerializer(ModelSerializer):
    class Meta:
        model = Map
        exclude = ('content', )
