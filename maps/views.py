# coding=utf-8

from __future__ import unicode_literals

from maps.models import Map
from maps.serializers import MapSerializer, MapListSerializer
from yago.views import BaseModelViewSet


class MapsViewSet(BaseModelViewSet):
    queryset = Map.objects.all()
    serializer_class = MapSerializer
    list_serializer_class = MapListSerializer

    def get_queryset(self):
        return super(MapsViewSet, self).get_queryset()\
            .filter(user=self.request.user)

    def create(self, request, *args, **kwargs):
        request.data['user'] = request.user.pk
        return super(MapsViewSet, self).create(request, *args, **kwargs)
