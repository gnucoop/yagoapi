# coding=utf-8

from __future__ import unicode_literals
from future.utils import python_2_unicode_compatible

from django.db import models

from jsonfield import JSONField

from yago.models import BaseModel, get_user_model_fk_ref


@python_2_unicode_compatible
class Map(BaseModel):
    user = models.ForeignKey(get_user_model_fk_ref())
    name = models.CharField(max_length=255)
    content = JSONField()

    def __str__(self):
        return self.name or ''
