# coding=utf-8

from __future__ import unicode_literals

from django.contrib.admin import ModelAdmin, register

from maps.models import Map


@register(Map)
class MapAdmin(ModelAdmin):
    pass
