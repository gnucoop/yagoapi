# coding=utf-8

from __future__ import unicode_literals

from rest_framework.routers import DefaultRouter

from maps.views import MapsViewSet


router = DefaultRouter()
router.register(r'maps', MapsViewSet, 'maps-maps')

urlpatterns = router.urls
