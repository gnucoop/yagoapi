import decimal

from django import VERSION
from django.conf import settings
from django.db import models


def get_user_model_fk_ref():
    version = decimal.Decimal('{v[0]}.{v[1]}'.format(v=VERSION))
    if version >= decimal.Decimal('1.5'):
        return settings.AUTH_USER_MODEL
    else:
        return 'auth.User'


class BaseModel(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    deleted = models.BooleanField(default=False, blank=True)

    def delete(self, using=None, keep_parents=False):
        self.deleted = True
        self.save()

    class Meta:
        abstract = True
