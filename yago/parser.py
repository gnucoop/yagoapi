# -*- coding: utf-8 -*-
import base64
import gzip
import json

from django.conf import settings

from rest_framework.parsers import JSONParser, ParseError, six

from djangorestframework_camel_case.util import underscoreize


class GzipCamelCaseJSONParser(JSONParser):
    def parse(self, stream, media_type=None, parser_context=None):
        parser_context = parser_context or {}
        encoding = parser_context.get('encoding', settings.DEFAULT_CHARSET)
        
        parser_context = parser_context or {}
        request = parser_context['request']
        gzipped = request.META.get('HTTP_CONTENT_ENCODING', '') == 'gzip'

        try:
            data = stream.read()
            if gzipped:
                data = gzip.decompress(base64.b64decode(data)).decode('utf-8')
            else:
                data = data.decode(encoding)
            return underscoreize(json.loads(data))
        except (ValueError, UnicodeDecodeError) as exc:
            raise ParseError('JSON parse error - %s' % six.text_type(exc))
