from django.conf import settings
from django.conf.urls import include, url
from django.conf.urls.static import static
from django.contrib import admin

from rest_framework_swagger.views import get_swagger_view


urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    url(r'^swagger/', get_swagger_view()),

    url(r'^accounts/', include('accounts.urls')),
    url(r'^layers/', include('layers.urls')),
    url(r'^maps/', include('maps.urls')),
    url(r'^gallery/', include('gallery.urls')),
]

if settings.DEBUG:
     urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
