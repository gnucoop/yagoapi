from rest_framework.viewsets import ModelViewSet


class BaseModelViewSet(ModelViewSet):
    list_serializer_class = None

    def get_queryset(self):
        return super(BaseModelViewSet, self).get_queryset().filter(deleted=False)

    def get_serializer_class(self):
        if self.action == 'list' and self.list_serializer_class is not None:
            return self.list_serializer_class
        return super(BaseModelViewSet, self).get_serializer_class()
