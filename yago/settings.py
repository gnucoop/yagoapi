# flake8: noqa

import os
import platform
import raven

from django.utils.translation import ugettext_lazy as _

from corsheaders.defaults import default_headers


BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

SECRET_KEY = '3va811bj+0q(k$bz@e#e92(eq09t1i2duo^pb6s*^t_-pes_8+'

DEBUG = True

ALLOWED_HOSTS = []

INSTALLED_APPS = [
    'accounts',
    'layers',
    'maps',
    'gallery',
    'yago',

    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.gis',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.sites',
    'allauth',
    'allauth.account',
    'rest_framework',
    'rest_framework_swagger',
    'corsheaders'
]

MIDDLEWARE = [
    'corsheaders.middleware.CorsMiddleware',
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
]

ROOT_URLCONF = 'yago.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

WSGI_APPLICATION = 'yago.wsgi.application'

DATABASES = {
    'default': {
        'ENGINE': 'django.contrib.gis.db.backends.postgis',
        'NAME': 'yago',
        'USER': 'yago',
        'PASSWORD': 'yago',
        'HOST': '127.0.0.1',
        'PORT': ''
    }
}

AUTHENTICATION_BACKENDS = (
    'django.contrib.auth.backends.ModelBackend',
    'allauth.account.auth_backends.AuthenticationBackend',
)

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True

STATIC_URL = '/static/'

LANGUAGES = [
    ('en', _('English')),
    ('fr', _('French'))
]

SITE_ID = 1

REST_FRAMEWORK = {
    'DEFAULT_AUTHENTICATION_CLASSES': (
        'rest_framework.authentication.SessionAuthentication',
        'rest_framework.authentication.BasicAuthentication',
        'rest_framework_jwt.authentication.JSONWebTokenAuthentication',
    ),
    'DEFAULT_RENDERER_CLASSES': (
        'djangorestframework_camel_case.render.CamelCaseJSONRenderer',
    ),
    'DEFAULT_PARSER_CLASSES': (
        'yago.parser.GzipCamelCaseJSONParser',
    ),
}

CORS_ORIGIN_WHITELIST = (
    'localhost:4200',
    'localhost:4201'
)

JWT_AUTH = {
    'JWT_ALLOW_REFRESH': True,
    'JWT_RESPONSE_PAYLOAD_HANDLER': 'accounts.utils.jwt_response_payload_handler'
}

if platform.system() == 'Darwin':
    SPATIALITE_LIBRARY_PATH = '/usr/local/lib/mod_spatialite.dylib'
else:
    SPATIALITE_LIBRARY_PATH = 'mod_spatialite'

if platform.system() == 'Windows':
    DATABASES = {
        'default': {
            'ENGINE': 'django.contrib.gis.db.backends.postgis',
            'NAME': 'yago',
            'USER': 'yago',
            'PASSWORD': 'yago',
            'HOST': '127.0.0.1',
            'PORT': ''
        }
    }

EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'
EMAIL_HOST = 'smtp.mailtrap.io'
EMAIL_PORT = 465
EMAIL_USE_TLS = True
EMAIL_HOST_USER = '23266e95ea9d39b3c'
EMAIL_HOST_PASSWORD = '7958e9c0724a5c'

FRONTEND_URL = 'http://127.0.0.1:4200'

MEDIA_ROOT = os.path.join(BASE_DIR, 'media')
GALLERY_PATH = 'gallery'
METADATA_PATH = 'metadata'
MEDIA_URL = '/media/'

CORS_ALLOW_HEADERS = default_headers + (
    'Content-encoding',
)
