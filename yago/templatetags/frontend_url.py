from django.conf import settings
from django.template import Library

register = Library()

@register.simple_tag
def frontend_url(*args):
    parts = []
    for a in args:
        if isinstance(a, (bytes, bytearray)):
            parts.append(a.decode())
        else:
            parts.append('{}'.format(a).strip('/'))
    return '{}/{}'.format(settings.FRONTEND_URL.strip('/'), '/'.join(parts))
