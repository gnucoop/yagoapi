# coding=utf-8

from __future__ import unicode_literals

import base64
import json

try:
  basestring
except NameError:
  basestring = str

from collections import OrderedDict

from django.contrib.gis.db.models import GeometryField
from django.contrib.gis.gdal import OGRException
from django.contrib.gis.geos import GEOSGeometry, GEOSException
from django.core.exceptions import ValidationError
from django.core.files.base import ContentFile
from django.utils.translation import ugettext_lazy as _

from rest_framework.fields import DictField, Field, FileField, empty
from rest_framework.serializers import ModelSerializer

from jsonfield import JSONField


class JsonDictField(Field):
    def to_representation(self, obj):
        return json.loads(obj)

    def to_internal_value(self, data):
        return json.dumps(data)


class B64FileField(FileField):
    def to_internal_value(self, data):
        file_data = None
        try:
            fmt, filestr = data['content'].split(';base64,')
            file_data = ContentFile(base64.b64decode(filestr), name=data['name'])
        except (AttributeError, ValueError, TypeError):
            file_data = data
        return super(B64FileField, self).to_internal_value(file_data)


class GeoJsonDict(OrderedDict):
    """
    Used for serializing GIS values to GeoJSON values
    TODO: remove this when support for python 2.6/2.7 will be dropped
    """

    def __str__(self):
        """
        Avoid displaying strings like
        ``{ 'type': u'Point', 'coordinates': [12, 32] }``
        in DRF browsable UI inputs (python 2.6/2.7)
        see: https://github.com/djangonauts/django-rest-framework-gis/pull/60
        """
        return json.dumps(self)


def geometry_to_geojson(value):
    if isinstance(value, dict) or value is None:
        return value
    gj = (
        ('type', value.geom_type),
        ('coordinates', value.coords),
    )
    if value.crs and value.crs.srid:
        crs = GeoJsonDict((
            ('type', 'EPSG'),
            ('properties', {'code': value.crs.srid}),
        ))
        gj += (('crs', crs), )
    
    return GeoJsonDict(gj)


class GeometryFieldSerializer(Field):
    type_name = 'GeometryField'

    def __init(self, **kwargs):
        super(GeometryField, self).__init__(**kwargs)
        self.style = {'base_template': 'textarea.html'}
    
    def to_representation(self, value):
        if isinstance(value, dict) or value is None:
            return value
        return geometry_to_geojson(value)
    
    def to_internal_value(self, value):
        if value == '' or value is None:
            return value
        if isinstance(value, GEOSGeometry):
            # value already has the correct representation
            return value
        if isinstance(value, dict):
            value = json.dumps(value)
        try:
            return GEOSGeometry(value)
        except (ValueError, GEOSException, OGRException, TypeError):
            raise ValidationError(_('Invalid format: string or unicode input unrecognized as GeoJSON, WKT EWKT or HEXEWKB.'))


class GeoModelSerializer(ModelSerializer):
    def __init__(self, instance=None, data=empty, **kwargs):
        super(GeoModelSerializer, self).__init__(instance=instance, data=data, **kwargs)
        self.serializer_field_mapping.update({
            GeometryField: GeometryFieldSerializer,
            JSONField: DictField
        })
