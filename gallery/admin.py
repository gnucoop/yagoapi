from django.contrib.admin import ModelAdmin, register

from gallery.models import GalleryItem, GalleryTheme


@register(GalleryTheme)
class GalleryThemeAdmin(ModelAdmin):
    pass


@register(GalleryItem)
class GalleryItemAdmin(ModelAdmin):
    pass
