# coding=utf-8

from __future__ import unicode_literals

from rest_framework.decorators import list_route

from gallery.models import GalleryItem, GalleryTheme
from gallery.serializers import GalleryItemSerializer, GalleryThemeSerializer
from yago.views import BaseModelViewSet


class GalleryThemeViewSet(BaseModelViewSet):
    queryset = GalleryTheme.objects.all()
    serializer_class = GalleryThemeSerializer
    parent_id = None

    def get_queryset(self):
        qs = super(GalleryThemeViewSet, self).get_queryset()
        if self.action == 'by_parent':
            return qs.filter(parent_id=self.parent_id)
        if self.action == 'by_parent_root':
            return qs.filter(parent=None)
        return qs

    @list_route(url_path='by_parent')
    def by_parent_root(self, request):
        self.parent_id = None
        return self.list(request)

    @list_route(url_path='by_parent/(?P<pid>\d+)')
    def by_parent(self, request, pid):
        self.parent_id = pid
        return self.list(request)


class GalleryItemViewSet(BaseModelViewSet):
    queryset = GalleryItem.objects.all()
    serializer_class = GalleryItemSerializer
    theme_id = None

    def get_queryset(self):
        qs = super(GalleryItemViewSet, self).get_queryset()
        if self.action == 'by_theme':
            return qs.filter(theme_id=self.theme_id)
        if self.action == 'by_theme_root':
            return qs.filter(theme=None)
        return qs

    @list_route(url_path='by_theme')
    def by_theme_root(self, request):
        self.parent_id = None
        return self.list(request)

    @list_route(url_path='by_theme/(?P<tid>\d+)')
    def by_theme(self, request, tid):
        self.theme_id = tid
        return self.list(request)
