# coding=utf-8

from __future__ import unicode_literals

from rest_framework.routers import DefaultRouter

from gallery.views import GalleryItemViewSet, GalleryThemeViewSet


router = DefaultRouter()
router.register(r'themes', GalleryThemeViewSet, 'gallery-themes')
router.register(r'items', GalleryItemViewSet, 'gallery-items')

urlpatterns = router.urls
