# coding=utf-8

from __future__ import unicode_literals
from future.utils import python_2_unicode_compatible

from django.conf import settings
from django.db import models
from django.utils.translation import ugettext_lazy as _

from yago.models import BaseModel

GALLERY_ITEM_TYPES = (
    ('image', _('Image'), ),
    ('document', _('Document'), ),
    ('archive', _('Archive'), ),
)


@python_2_unicode_compatible
class GalleryTheme(BaseModel):
    name = models.CharField(max_length=255)
    parent = models.ForeignKey('gallery.GalleryTheme', blank=True, null=True, default=None)

    @property
    def parent_name(self):
        return self.parent.name if self.parent is not None else ''

    def __str__(self):
        return self.name


@python_2_unicode_compatible
class GalleryItem(BaseModel):
    item_type = models.CharField(choices=GALLERY_ITEM_TYPES, max_length=20)
    theme = models.ForeignKey('gallery.GalleryTheme')
    name = models.CharField(max_length=255)
    description = models.TextField()
    attachment = models.FileField(upload_to=settings.GALLERY_PATH)

    def __str__(self):
        return self.name
