# coding=utf-8

from __future__ import unicode_literals

from rest_framework.fields import CharField
from rest_framework.serializers import ModelSerializer

from gallery.models import GalleryItem, GalleryTheme
from yago.serializers import B64FileField


class GalleryThemeSerializer(ModelSerializer):
    parent_name = CharField(read_only=True)

    class Meta:
        model = GalleryTheme
        fields = '__all__'


class GalleryItemSerializer(ModelSerializer):
    attachment = B64FileField(required=False, allow_null=True)

    class Meta:
        model = GalleryItem
        fields = '__all__'
