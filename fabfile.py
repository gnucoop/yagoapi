import os
from fabric.api import env, require, run, sudo, cd

envs = [
    {'webapps_root': '/home/yago', 'supervisor_script': 'yago'},
    {'webapps_root': '/home/haiti-sud-est', 'supervisor_script': 'haiti-sud-est'},
    {'webapps_root': '/home/guatemala', 'supervisor_script': 'guatemala'},
    {'webapps_root': '/home/haiti-ouest', 'supervisor_script': 'haiti-ouest'}
]

def initenv(env_num):
    env.project_name = 'yagoapi'
    env.server_name = 'yagodev'
    env.webapps_root = envs[env_num]['webapps_root']
    env.supervisor_script = envs[env_num]['supervisor_script']
    env.project_root = os.path.join(env.webapps_root, env.project_name)
    env.activate_script = os.path.join(env.webapps_root, 'env/bin/activate')
    env.wsgi_file = os.path.join(env.project_root, 'yago.wsgi')
    env.repo_root = env.project_root
    env.requirements_file = os.path.join(env.repo_root, 'requirements.txt')
    env.manage_dir = env.repo_root


def development():
    env.hosts = [env.server_name]


def virtualenv(command, use_sudo=False):
    if use_sudo:
        func = sudo
    else:
        func = run
    func('source "%s" && %s' % (env.activate_script, command))


def update():
    require('hosts', provided_by=[development])
    with cd(env.repo_root):
        run('git pull origin master')


def install_requirements():
    require('hosts', provided_by=[development])
    virtualenv('pip install -q -r %(requirements_file)s' % env)


def manage_py(command, use_sudo=False):
    require('hosts', provided_by=[development])
    with cd(env.manage_dir):
        virtualenv('python manage.py %s' % command, use_sudo)


def migrate():
    require('hosts', provided_by=[development])
    manage_py('migrate --noinput')


def collectstatic():
    require('hosts', provided_by=[development])
    manage_py('collectstatic -l --noinput')


def reload():
    require('hosts', provided_by=[development])
    sudo('supervisorctl status | grep %(supervisor_script)s '
         '| sed "s/.*[pid ]\([0-9]\+\)\,.*/\\1/" '
         '| xargs kill -HUP' % env)


def deploy(env_num):
    initenv(env_num)
    require('hosts', provided_by=[development])
    update()
    install_requirements()
    migrate()
    collectstatic()
    reload()

def deploy_peru():
    deploy(0)

def deploy_haiti_sud_est():
    deploy(1)

def deploy_guatemala():
    deploy(2)

def deploy_haiti_ouest():
    deploy(3)
